import React, { useState } from "react";
import "./App.css";

function App() {
  const [text, setText] = useState("");
  const [chats, setChats] = useState([]);
  const onTextChange = (e) => {
    setText(e.target.value);
  };
  const showChats = () => {
    console.log(JSON.parse(localStorage.getItem("chats")));
  };
  const onTextSubmit = (e) => {
    e.preventDefault();
    setChats(chats.concat(text));
    localStorage.setItem("chats", JSON.stringify(chats));
  };
  return (
    <div className="App">
      <input id="example" value={text} onChange={onTextChange}></input>
      <button onClick={onTextSubmit}>Send</button>
      <button onClick={showChats}>Show</button>
    </div>
  );
}

export default App;
